#pragma once

template <typename T>
class Stack
{
public:

	// ---------------------------------------------------------------
	//                CONSTRUCTOR / COPY / DESTRUCTOR
	// ---------------------------------------------------------------

	explicit Stack(size_t _size = DEFAULT) : size(_size)
	{ 
		data = new T[size]; 
	}
	
	Stack(const Stack& st) : size(st.size), head(st.head)
	{
		data = new T[size];
		for (size_t i = 0U; i < head; ++i)
			data[i] = st.data[i];
	}

	Stack& operator=(const Stack& st)
	{
		{
			if (this == &st)
				return *this;

			size = st.size;
			head = st.head;

			delete[] data;
			data = new T[size];
			for (size_t i = 0U; i < head; ++i)
				data[i] = st.data[i];

			return *this;
		}
	}


	~Stack() { delete[] data; }

	// ---------------------------------------------------------------
	//                           UTILITY
	// ---------------------------------------------------------------

	bool Empty() const { return head == 0; }
	bool Full() const { return head == size; }

	size_t Size() const { return head; }
	size_t MaxSize() const { return size; }

	bool Push(const T& item)
	{
		if (head >= size && !Grow())
			return false;
		data[head++] = item;
		return true;
	}

	bool Pop(T& item)
	{
		if (head == 0U)
			return false;
		item = data[--head];
		return true;
	}

	bool Pop()
	{
		if (head == 0U)
			return false;
		--head;
		return true;
	}

	// Shrink() reduce Size to an actual foorprint, determined by Head
	bool Shrink()
	{
		if (head >= size) // if already shrinked -- exit
			return false;
		else {
			T* temp = new (std::nothrow) T[head];
			if (temp == nullptr)
				return false;
			for (size_t i = 0U; i < head; ++i)
				temp[i] = data[i];
			delete[] data;
			data = temp;
			size = head;
			return true;
		}
	}

	const T* Data() const
	{
		return data;
	}


protected:
	// Try fitting more elements by moving to a NEW space
	// Grow() increase size by a factor of 2 (eq 10 -> 20 -> 40) 
	// In theory we might get overflow someday, but it should be OK
	bool Grow()
	{
		const size_t newsize = (size > 0U ? size * 2U : 1U); 
		T* temp = new (std::nothrow) T[newsize]; // might need safety logic
		if (temp == nullptr)
			return false;
		for (size_t i = 0U; i < head; ++i) // copying elements to a new location
			temp[i] = data[i];
		delete[] data;
		data = temp;
		size = newsize;
		return true;
	}

private:

	// ---------------------------------------------------------------
	//                           DATA
	// ---------------------------------------------------------------

	static constexpr size_t DEFAULT = 10U; // - DEFAULT Stack size

	size_t head{ 0U };
	size_t size{ 0U };

	T* data{ nullptr };
};