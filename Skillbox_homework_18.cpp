#include <iostream>
#include <string>
#include "Stack.h"

int main()
{
    // Stack functionality test
    Stack<char> Test1; // implicit initialization with size=10

    std::cout << "Stack default capacity is " << Test1.MaxSize() << " elements\n";
    
    auto Word = "Skillbox";
    for (size_t i = 0U; Word[i]; ++i)
        Test1.Push(Word[i]);

    Stack<char> Test2{ 3U }; // explicit initialization with size=3

    for (char ch; Test1.Pop(ch); Test2.Push(ch))
        std::cout << "Test2 capacity is " << Test2.MaxSize() << ", head points to " << Test2.Size() << ", memory location = " << (int*)Test2.Data() << '\n';
    std::cout << "Test2 capacity is " << Test2.MaxSize() << ", head points to " << Test2.Size() << ", memory location = " << (int*)Test2.Data() << '\n';
    std::cout << "Test2 Shrink()ed\n"; 
    Test2.Shrink();
    std::cout << "Test2 capacity is " << Test2.MaxSize() << ", head points to " << Test2.Size() << ", memory location = " << (int*)Test2.Data() << '\n';

    std::cout << "\nInput Word was \"" << Word << "\", but after stacking letters in and out twice it has become... ";
    
    for (char ch; Test2.Pop(ch); std::cout << ch) {}

    std::cout << " again! \nWell done, Stack!\n";

    // String test
    Stack<std::string> Women;
    Women.Push("Yennefer");
    Women.Push("Triss");
    Women.Push("Shani");

    std::cout << "\nWhich lady funbase loves the most?\n";

    for (std::string str; Women.Pop(str); std::cout << (Women.Empty()? "?" : "? Or is it "))
        std::cout << str;
    std::cout << '\n';

    return 0;
}
